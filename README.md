**coding style**

http://httpd.apache.org/dev/styleguide.html

    $ indent -i4 -npsl -di0 -br -nce -d0 -cli0 -npcs -nfc1 -nut
    $ cppcheck --enable=all

**usage**

	$ make
	$ ./deadlock_pthread_with_fork_ex
    check deadlock fork process
    $ ps
    $ killall deadlock_pthread_with_fork_ex
