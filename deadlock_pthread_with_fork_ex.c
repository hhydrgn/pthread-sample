#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)

#include <pthread.h>

void *deadlock_test(void *arg)
{
    static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&mutex);
    usleep(1000);
    pthread_mutex_unlock(&mutex);
    return 0;
}

int main()
{
    debug_log("entered %s\n", __FILE__);

    pthread_t t;
    pthread_create(&t, 0, deadlock_test, 0);

    pid_t pid = fork();

    if (pid < 0) {
        perror("fork()");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        debug_log("start child process\n");
        deadlock_test(0);
        debug_log("finish child process\n");
        return 0;
    }
    pthread_join(t, 0);

    debug_log("exit %s\n", __FILE__);

    return 0;
}
