targets = deadlock_pthread_with_fork_ex

all: $(targets)

%: %.c
	gcc -g -lpthread -o $@ $<

clean:
	rm -f $(targets)
